package selenium_api;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_08_JavascriptExecutor extends CommonMethod {

	@BeforeClass
	public void tearUp() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", ".\\drivers\\Windows\\chromedriver.exe");
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					".\\drivers\\Mac\\chromedriver.exe".replaceAll("\\", File.separator));
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test
	public void TS01() {
		openUrl("http://live.guru99.com/");
		compareText(jsExecute("return document.domain;").toString(), "live.guru99.com");
		compareText(jsExecute("return document.URL;").toString(), "http://live.guru99.com/");
		clickByJS(By.xpath("//a[contains(text(),'Mobile')]"));
		clickByJS(By
				.xpath("//h2[a[contains(text(),'Samsung Galaxy')]]/following-sibling::div[@class='actions']//button"));
		compareText(
				jsExecute("return arguments[0].innerText;",
						driver.findElement(By.xpath("//li[@class='success-msg']//span"))).toString(),
				"Samsung Galaxy was added to your shopping cart.");
		clickByJS(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		compareText(jsExecute("return document.title;").toString(), "Privacy Policy");
		jsExecute("window.scrollBy(0,document.body.scrollHeight)");
		jsExecute("document.location = 'http://demo.guru99.com/v4/'");
		compareText(jsExecute("return document.domain;").toString(), "demo.guru99.com");

	}

	@Test
	public void TS02_RemoveAttribute() {
		openUrl("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='iframeResult']")));
		jsExecute("arguments[0].removeAttribute('disabled');", driver.findElement(By.xpath("//input[@name='lname']")));
		driver.findElement(By.xpath("//input[@name='lname']")).sendKeys("Hello world");
		click(By.xpath("//input[@type='submit']"));

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
