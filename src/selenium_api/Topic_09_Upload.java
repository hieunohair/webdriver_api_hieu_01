package selenium_api;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class Topic_09_Upload extends CommonMethod {

	@BeforeClass
	public void tearUp() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", ".\\drivers\\Windows\\chromedriver.exe");
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					".\\drivers\\Mac\\chromedriver.exe".replaceAll("\\", File.separator));
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test
	public void TS01_UploadFileBySendkeys() {
		String url = new File(".\\AutoIT\\000041.JPG").getAbsolutePath();
		By by = By.xpath("//input[@type='file'");
		openUrl("http://blueimp.github.io/jQuery-File-Upload/");
		uploadBySendkey(url, by);
		verifyText(By.xpath("//p[@class='name']"), "000041.JPG");
	}

	@Test
	public void TS02_UploadFileByAutoIT() throws IOException {
		String url = new File(".\\AutoIT\\000041.JPG").getAbsolutePath();
		By by = By.xpath("//input[@type='file'");
		openUrl("http://blueimp.github.io/jQuery-File-Upload/");
		uploadByAutoIT(url, by);
		verifyText(By.xpath("//p[@class='name']"), "000041.JPG");
	}

	@Test
	public void TS03_UploadFileByRobot() throws InterruptedException, AWTException {
		String url = new File(".\\AutoIT\\000041.JPG").getAbsolutePath();
		By by = By.xpath("//input[@type='file'");
		openUrl("http://blueimp.github.io/jQuery-File-Upload/");
		uploadByRobot(url, by);
		verifyText(By.xpath("//p[@class='name']"), "000041.JPG");
	}

	@Test
	public void TS04_UploadFile() throws InterruptedException {
		File a = new File(".\\AutoIT\\000041.JPG");
		openUrl("https://encodable.com/uploaddemo/");
		By byUpload = By.xpath("//input[@id='uploadname1']");
		uploadBySendkey(a.getAbsolutePath(), byUpload);
		SelectByText(By.xpath("//select[@name='subdir1']"), "/uploaddemo/files/");
		String folder = "HieuTest" + new Random().nextInt();
		inputText(By.xpath("//input[@id='newsubdir1']"), folder);
		inputText(By.xpath("//input[@id='formfield-email_address']"), "dam@gmail.com");
		inputText(By.xpath("//input[@id='formfield-first_name']"), "DAM DAO");
		click(By.xpath("//input[@id='uploadbutton']"));
		new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebDriver, WebElement>() {
					public WebElement apply(WebDriver driver) {
						return driver.findElement(By.xpath("//dt[text()='Your upload is complete:']"));
					}

				});

		verifyText(By.xpath("//dd[contains(text(),'Email Address')]"), "Email Address: dam@gmail.com");
		verifyText(By.xpath("//dd[contains(text(),'First Name')]"), "First Name: DAM DAO");
		click(By.xpath("//a[contains(text(),'View Uploaded Files')]"));
		click(By.xpath("//a[contains(text(),'" + folder + "')]"));
		verifyText(By.xpath("//a[contains(@id,'fclink')]"), "000041.JPG");

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
