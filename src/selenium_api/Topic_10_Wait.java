package selenium_api;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class Topic_10_Wait extends CommonMethod {

	@BeforeClass
	public void tearUp() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", ".\\drivers\\Windows\\chromedriver.exe");
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					".\\drivers\\Mac\\chromedriver.exe".replaceAll("\\", File.separator));
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@Test
	public void TS01_ImplicitWait() {
		openUrl("http://the-internet.herokuapp.com/dynamic_loading/2");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		click(By.xpath("//button[contains(text(),'Start')]"));
		verifyText(By.xpath("//div[@id='finish']/h4"), "Hello World!");

	}

	@Test
	@SuppressWarnings("deprecation")
	public void TS02_ExplicitWait() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		driver.manage().timeouts().pageLoadTimeout(600, TimeUnit.SECONDS);
		openUrl("http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='example']")));
		verifyText(By.xpath("//span[@id='ctl00_ContentPlaceholder1_Label1']"), "No Selected Dates to display.");
		Date date = new Date();
		click(By.xpath("//a[@href='#'][(text()='" + date.getDate() + "')]"));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='raDiv']")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[contains(@class,'rcSelected')]//a[text()='" + date.getDate() + "']")));
		verifyText(By.xpath("//span[@id='ctl00_ContentPlaceholder1_Label1']"),
				new SimpleDateFormat("EEEE, MMMM dd, yyyy").format(date));
	}

	@Test
	public void TS03_FluentWait() {
		openUrl("https://stuntcoders.com/snippets/javascript-countdown/");
		new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebDriver, Boolean>() {
					public Boolean apply(WebDriver driver) {
						String time = driver.findElement(By.xpath("//div[@id='javascript_countdown_time']")).getText();
						return time.endsWith("00");
					}
				});
	}

	@Test
	public void TS04_FluentWait() {
		openUrl("http://toolsqa.wpengine.com/automation-practice-switch-windows/");
		new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebDriver, Boolean>() {
					public Boolean apply(WebDriver driver) {
						return driver.findElement(By.id("colorVar")).getAttribute("style")
								.equalsIgnoreCase("color: red;");
					}

				});
		new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(10, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class).until(new Function<WebDriver, Boolean>() {
					public Boolean apply(WebDriver driver) {
						return driver.findElement(By.id("clock")).getText().
								equalsIgnoreCase("Buzz Buzz");
					}

				});
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
