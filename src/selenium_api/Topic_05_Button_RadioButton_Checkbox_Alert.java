package selenium_api;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_05_Button_RadioButton_Checkbox_Alert extends CommonMethod {
	@BeforeClass
	public void tearup() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Windows/chromedriver.exe".replaceAll("/", File.separator));
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Mac/chromedriver.exe".replaceAll("/", File.separator));
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test
	public void TS01_JSClick() {
		openUrl("http://live.guru99.com/");
		click(By.xpath("//div[@class='footer']//a[contains(text(), 'My Account')]"));
		verifyUrl("http://live.guru99.com/index.php/customer/account/login/");
		clickByJS(By.xpath("//span[contains(text(),'Create an Account')]"));
		verifyUrl("http://live.guru99.com/index.php/customer/account/create/");
	}

	@Test
	public void TS02_SelectCheckbox() {
		openUrl("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		click(By.xpath("//label[contains(text(),'Dual-zone air conditioning')]"));
		verifySelected(By.xpath("//label[contains(text(),'Dual-zone air conditioning')]/../input"));
		click(By.xpath("//label[contains(text(),'Dual-zone air conditioning')]"));
		verifyDeselected(By.xpath("//label[contains(text(),'Dual-zone air conditioning')]/../input"));
	}

	@Test
	public void TS03_SelectRadioButton() {
		openUrl("http://demos.telerik.com/kendo-ui/styling/radios");
		do {
			click(By.xpath("//label[contains(text(),'2.0 Petrol, 147kW')]"));
			verifySelected(By.xpath("//label[contains(text(),'2.0 Petrol, 147kW')]/../input"));
		} while (!isSelected(By.xpath("//label[contains(text(),'2.0 Petrol, 147kW')]/../input")));
	}

	@Test
	public void TS04_JSAlert() {
		openUrl("http://daominhdam.890m.com/");
		click(By.xpath("//button[@onclick='jsAlert()']"));
		Alert alert = driver.switchTo().alert();
		compareText(alert.getText(), "I am a JS Alert");
		alert.accept();
		System.out.println("You clicked an alert successfully");
	}

	@Test
	public void TS05_JSConfirm() {
		openUrl("http://daominhdam.890m.com/");
		click(By.xpath("//button[@onclick='jsConfirm()']"));
		Alert alert = driver.switchTo().alert();
		compareText(alert.getText(), "I am a JS Confirm");
		alert.dismiss();
		System.out.println("You clicked: Cancel");
	}

	@Test
	public void TS06_JSPromt() {
		openUrl("http://daominhdam.890m.com/");
		click(By.xpath("//button[@onclick='jsPrompt()']"));
		Alert alert = driver.switchTo().alert();
		compareText(alert.getText(), "I am a JS prompt");
		String s = "This is a fucking stupid text";
		alert.sendKeys(s);
		alert.accept();
		System.out.println(" You entered: " + s);
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}
}
