package selenium_api;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_07_iFrameAndWindow extends CommonMethod {

	@BeforeClass
	public void tearUp() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver",
					".\\drivers\\Windows\\chromedriver.exe");
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					".\\drivers\\Mac\\chromedriver.exe".replaceAll("\\", File.separator));
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	// @Test
	public void TS01_() {
		openUrl("http://www.hdfcbank.com/");
		if (!(driver.findElements(By.xpath("//iframe[@id='vizury-notification-template']")).size() == 0)) {
			driver.switchTo().frame(0);
			click(By.xpath("//div[@id='div-close']"));
		}
	}

	// @Test
	public void TS02_WindowHandle() {
		openUrl("http://daominhdam.890m.com/");
		click(By.xpath("//a[contains(text(),'Click Here')]"));
		switchToNewWindow();
		verifyTitle("Google");
		closeOtherWindows();
	}

	@Test
	public void TS03_IframeControl() {
		openUrl("http://www.hdfcbank.com/");
		if (!(driver.findElements(By.xpath("//iframe[@id='vizury-notification-template']")).size() == 0)) {
			driver.switchTo().frame(0);
			click(By.xpath("//div[@id='div-close']"));
		}
		String title = driver.getTitle();
		click(By.xpath("//div[@class='sectionnav']//a[contains(text(),'Agri')]"));
		switchToWindowByTitle("HDFC Bank Kisan Dhan Vikas e-Kendra ");
		click(By.xpath("//p[contains(text(),'Account Details')]"));
		switchToWindowByTitle("Welcome to HDFC Bank NetBanking");
		driver.switchTo().frame(driver.findElement(By.xpath("//frame[@name='footer']")));
		click(By.xpath("//a[text()='Privacy Policy']"));
		switchToWindowByTitle(
				"HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan");
		click(By.xpath("//a[@title='Corporate Social Responsibility']"));
		switchToWindowByTitle(title);
		closeOtherWindows();
	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
