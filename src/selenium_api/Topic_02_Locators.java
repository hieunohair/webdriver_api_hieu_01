package selenium_api;

import java.io.File;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_02_Locators {
	WebDriver driver;

	@BeforeClass
	public void tearup() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Windows/chromedriver.exe".replaceAll("/", File.separator));
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Mac/chromedriver.exe".replaceAll("/", File.separator));
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	/*
	 * Step 01 - Truy cập vào trang: http://live.guru99.com Step 02 - Kiểm tra title
	 * của page là: "Home page" Step 03 - Click vào link "My Account" để tới trang
	 * đăng nhập Step 04 - Click CREATE AN ACCOUNT button để tới trang đăng kí tài
	 * khoản Step 05 - Back lại trang đăng nhập Step 06 - Kiểm tra url của page đăng
	 * nhập là: http://live.guru99.com/index.php/customer/account/login/ Step 07 -
	 * Forward tới trang tạo tài khoản Step 08 - Kiểm tra url của page tạo tài khoản
	 * là: http://live.guru99.com/index.php/customer/account/create/
	 */
	@Test
	public void TS01_VerifyURLAndTitle() {
		driver.get("http://live.guru99.com");
		Assert.assertEquals(driver.getTitle(), "Home page");
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(), 'My Account')]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Create an Account')]")).click();
		driver.navigate().back();
		Assert.assertEquals(driver.getCurrentUrl(), "http://live.guru99.com/index.php/customer/account/login/");
		driver.navigate().forward();
		Assert.assertEquals(driver.getCurrentUrl(), "http://live.guru99.com/index.php/customer/account/create/");
	}

	/*
	 * Step 01 - Truy cập vào trang: http://live.guru99.com/ Step 02 - Click vào
	 * link "My Account" để tới trang đăng nhập Step 03 - Để trống Username/
	 * Password Step 04 - Click Login button Step 05 - Verify error message xuất
	 * hiện tại 2 field:
	 */
	@Test
	public void TS02_LoginEmpty() {
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(), 'My Account')]")).click();
		driver.findElement(By.xpath("//input[@id='email']")).clear();
		driver.findElement(By.xpath("//input[@id='pass']")).clear();
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText(),
				"This is a required field.");
		Assert.assertEquals(driver.findElement(By.xpath("//div[@id='advice-required-entry-pass']")).getText(),
				"This is a required field.");
	}

	/*
	 * Step 01 - Truy cập vào trang: http://live.guru99.com/ Step 02 - Click vào
	 * link "My Account" để tới trang đăng nhập Step 03 - Nhập email invalid:
	 * 123434234@12312.123123 Step 04 - Click Login button Step 05 - Verify error
	 * message xuất hiện: Please enter a valid email address. For example
	 * johndoe@domain.com.
	 */
	@Test
	public void TS03_LoginWithEmailInvalid() {
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(), 'My Account')]")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("123434234@12312.123123");
		driver.findElement(By.xpath("//input[@id='pass']")).clear();
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[@id='advice-validate-email-email']")).getText(),
				"Please enter a valid email address. For example johndoe@domain.com.");

	}

	/*
	 * Step 01 - Truy cập vào trang: http://live.guru99.com/ Step 02 - Click vào
	 * link "My Account" để tới trang đăng nhập Step 03 - Nhập email correct and
	 * password incorrect: automation@gmail.com/ 123 Step 04 - Click Login button
	 * Step 05 - Verify error message xuất hiện: Please enter 6 or more characters
	 * without leading or trailing spaces.
	 */
	@Test
	public void TS04_LoginWithPasswordInvalid() {
		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(), 'My Account')]")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("automation@gmail.com");
		;
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("123");
		driver.findElement(By.xpath("//button[@id='send2']")).click();
		Assert.assertEquals(driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']")).getText(),
				"Please enter 6 or more characters without leading or trailing spaces.");

	}

	/*
	 * Step 01 - Truy cập vào trang: http://live.guru99.com/ Step 02 - Click vào
	 * link "My Account" để tới trang đăng nhập Step 03 - Click CREATE AN ACCOUNT
	 * button để tới trang đăng kí tài khoản Step 04 - Nhập thông tin hợp lệ vào tất
	 * cả các field: First Name/ Last Name/ Email Address/ Password/ Confirm
	 * Password (Lưu ý: Tạo random cho dữ liệu tại field Email Address) Step 05 -
	 * Click REGISTER button Step 06 - Verify message xuất hiện khi đăng kí thành
	 * công: Thank you for registering with Main Website Store. Step 07 - Logout
	 * khỏi hệ thống Step 08 - Kiểm tra hệ thống navigate về Home page sau khi
	 * logout thành công
	 */
	@Test
	public void TS05_CreateAnAccount() throws InterruptedException {

		driver.get("http://live.guru99.com/");
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(text(), 'My Account')]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Create an Account')]")).click();
		String email = new Random().nextInt(1000) + "automation@gmail.com";
		System.out.println(email);
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("Hieu");
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("Nguyen");
		driver.findElement(By.xpath("//input[@id='email_address']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("123456");
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys("123456");
		driver.findElement(By.xpath("//span[contains(text(),'Register')]")).click();
		Assert.assertEquals(driver.findElement(By.xpath("//li[@class='success-msg']//span")).getText(),
				"Thank you for registering with Main Website Store.");
		driver.findElement(By.xpath("//span[@class='label'][contains(text(),'Account')]")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Log Out')]")).click();
		Thread.sleep(5000);
		Assert.assertEquals(driver.getTitle(), "Home page");
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}
}
