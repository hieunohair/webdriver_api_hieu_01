package selenium_api;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_06_Actions extends CommonMethod {

	@BeforeClass
	public void tearUp() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Windows/chromedriver.exe".replaceAll("/", File.separator));
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Mac/chromedriver.exe".replaceAll("/", File.separator));
		}

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test
	public void TS01_Hover() {
		openUrl("http://daominhdam.890m.com/");
		mouseOver(By.xpath("//a[contains(text(),'Hover over me')]"));
		assertEquals(isDisplayed(By.xpath("//div[@class='tooltip-inner']")), true);
		verifyText(By.xpath("//div[@class='tooltip-inner']"), "Hooray!");
	}

	@Test
	public void TS01_HoverThenClick() {
		openUrl("http://www.myntra.com/");
		mouseOver(By.xpath("//div[@class='desktop-userIconsContainer']"));
		click(By.xpath("//a[contains(text(),'login')]"));
		Assert.assertEquals(isDisplayed(By.xpath("//div[@class='login-box']")), true);
	}

	@Test
	public void TS02_ClickAndHold() {
		openUrl("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		List<By> by = new ArrayList<>();
		by.add(By.xpath("//li[text()=1]"));
		by.add(By.xpath("//li[text()=4]"));
		clickAndHoldAndMove(by);
		Assert.assertEquals(
				driver.findElements(By.xpath("//li[@class='ui-state-default ui-selectee ui-selected']")).size(), 4);
	}

	@Test
	public void TS03_DoubleClick() {
		openUrl("http://www.seleniumlearn.com/double-click");
		doubleClick(By.xpath("//button[contains(text(),'Double-Click Me!')]"));
		Alert alert = driver.switchTo().alert();
		compareText(alert.getText(), "The Button was double-clicked.");
		alert.accept();
	}

	@Test
	public void TS04_RightClick() {
		openUrl("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		rightClick(By.xpath("//span[text()='right click me']"));
		mouseOver(By.xpath("//span[text()='Quit']"));
		Assert.assertEquals(isDisplayed(By.xpath(
				"//li[contains(@class,'context-menu-visible') and contains(@class,'context-menu-hover')]/span[text()='Quit']")),
				true);
		click(By.xpath("//span[text()='Quit']"));
		driver.switchTo().alert().accept();
	}

	@Test
	public void TS05_DragAndDrop1() {
		openUrl("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		dragAdnDrop(By.xpath("//div[@id='draggable']"), By.xpath("//div[@id='droptarget']"));
		verifyText(By.xpath("//div[@id='droptarget']"), "You did great!");
	}

	@Test
	public void TS05_DragAndDrop2() {
		openUrl("http://jqueryui.com/resources/demos/droppable/default.html");
		dragAdnDrop(By.xpath("//div[@id='draggable']"), By.xpath("//div[@id='droppable']"));
		verifyText(By.xpath("//div[@id='droppable']"), "Dropped!");
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}
}
