package selenium_api;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CommonMethod {
	WebDriver driver;

	public void compareText(String txt1, String txt2) {
		Assert.assertEquals(txt1, txt2);
	}

	public void verifyText(By by, String expectation) {
		compareText(driver.findElement(by).getText(), expectation);
	}

	public void verifyAttribute(By by, String attribute, String expectation) {
		compareText(driver.findElement(by).getAttribute(attribute), expectation);
	}

	public void verifyUrl(String expectation) {
		compareText(driver.getCurrentUrl(), expectation);
	}

	public void verifyTitle(String expectation) {
		compareText(driver.getTitle(), expectation);
	}

	public void click(By by) {
		driver.findElement(by).click();
	}

	public Object jsExecute(String jsFunction, Object obj) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeScript(jsFunction, obj);
	}
	public Object jsExecute(String jsFunction) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeScript(jsFunction);
	}
	public void clickByJS(By by) {
		jsExecute("arguments[0].click();", driver.findElement(by));
	}

	public void openUrl(String url) {
		driver.get(url);
	}

	public boolean isSelected(By by) {
		return driver.findElement(by).isSelected();
	}

	public boolean isDisplayed(By by) {
		return driver.findElement(by).isDisplayed();
	}

	public void verifySelected(By by) {
		Assert.assertEquals(isSelected(by), true);
	}

	public void verifyDeselected(By by) {
		Assert.assertEquals(isSelected(by), false);
	}

	public void mouseOver(By by) {
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(by)).perform();
	}

	public void clickAndHold(By by) {
		Actions action = new Actions(driver);
		action.clickAndHold(driver.findElement(by)).perform();
	}

	public void clickAndHoldAndMove(List<By> by) {
		Actions action = new Actions(driver);
		action.clickAndHold(driver.findElement(by.get(0)));
		for (int i = 1; i < by.size(); i++) {
			action.moveToElement(driver.findElement(by.get(1))).perform();
		}
	}

	public void doubleClick(By by) {
		Actions action = new Actions(driver);
		action.doubleClick(driver.findElement(by)).perform();
	}

	public void rightClick(By by) {
		Actions action = new Actions(driver);
		action.contextClick(driver.findElement(by)).perform();
	}

	public void dragAdnDrop(By source, By target) {
		Actions action = new Actions(driver);
		action.dragAndDrop(driver.findElement(source), driver.findElement(target)).perform();
	}

	public void switchToWindowByTitle(String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			driver.switchTo().window(runWindows);
			String currentWin = driver.getTitle();
			if (currentWin.equals(title)) {
				break;
			}
		}
	}

	public void switchToNewWindow() {
		String OldWindows = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(OldWindows)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	public void closeOtherWindows() {
		String OldWindows = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(OldWindows)) {
				driver.switchTo().window(runWindow);
				driver.close();
			}
		}
		driver.switchTo().window(OldWindows);
	}

	public void switchThenCloseOtherWindows() {
		String OldWindows = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(OldWindows)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
		OldWindows = driver.getWindowHandle();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(OldWindows)) {
				driver.switchTo().window(runWindow);
				driver.close();
			}
		}
		driver.switchTo().window(OldWindows);
	}
	public void uploadBySendkey(String upload, By by){
		driver.findElement(by).sendKeys(upload);
	}
	public void uploadByAutoIT(String upload, By by) throws IOException{
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();
		if (browserName.toLowerCase().contains("chrome")){
			Runtime.getRuntime().exec(new String[] { ".\\AutoIT\\chrome.exe", upload });
		}
		else if (browserName.toLowerCase().contains("firefox")){
			Runtime.getRuntime().exec(new String[] { ".\\AutoIT\\firefox.exe", upload });
		} 
		else if (browserName.toLowerCase().contains("ie")){
			Runtime.getRuntime().exec(new String[] { ".\\AutoIT\\ie.exe", upload });
		}
		click(by);
	}
	public void uploadByRobot(String upload, By by) throws InterruptedException, AWTException{
		click(by);	
		//Specify the file location with extension
		StringSelection select = new StringSelection(upload);
		//Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);
		//Click			
		Robot robot = new Robot();
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}
	public void SelectByText(By by, String value){
		Select select = new Select(driver.findElement(by));
		select.selectByVisibleText(value);
	}
	public void inputText(By by, String value){
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(value);
	}
}
