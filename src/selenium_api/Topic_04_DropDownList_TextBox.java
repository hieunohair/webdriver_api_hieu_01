package selenium_api;

import java.io.File;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_04_DropDownList_TextBox {
	WebDriver driver;

	@BeforeClass
	public void tearup() {
		if (System.getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Windows/chromedriver.exe".replaceAll("/", File.separator));
		} else if (System.getProperty("os.name").contains("Mac")) {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/Mac/chromedriver.exe".replaceAll("/", File.separator));
		}
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Test
	public void TS01_Dropdownlist() {
		driver.get("http://daominhdam.890m.com/");
		Select sl = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertEquals(sl.isMultiple(), false);
		sl.selectByVisibleText("Automation Tester");
		Assert.assertEquals(sl.getFirstSelectedOption().getText(), "Automation Tester");
		sl.selectByValue("manual");
		Assert.assertEquals(sl.getFirstSelectedOption().getText(), "Manual Tester");
		sl.selectByIndex(3);
		Assert.assertEquals(sl.getFirstSelectedOption().getText(), "Mobile Tester");
		Assert.assertEquals(sl.getOptions().size(), 5);
	}

	@Test
	public void TS02_TextBox_AreaBox() {
		String email = "mngr143067";
		String pw = "qUbEdUh";
		// Access site
		driver.get("http://demo.guru99.com/v4/");
		// Login
		driver.findElement(By.xpath("//input[@name='uid']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pw);
		driver.findElement(By.xpath("//input[@value='LOGIN']")).click();
		// Open New Customer
		driver.findElement(By.xpath("//a[contains(text(),'New Customer')]")).click();
		// Input data
		String cname = "Hieu";
		String gender = (new Random().nextInt(1) == 1) ? "m" : "f";
		String address = "I dont know";
		String city = "ANNAPOLIS";
		email = new Random().nextInt(10000) + email + "@test.com";
		driver.findElement(By.xpath("//input[@name='name']")).sendKeys(cname);
		driver.findElement(By.xpath("//input[@value='" + gender + "']")).click();
		driver.findElement(By.xpath("//input[@id='dob']")).sendKeys("02/08/1990");
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys(address);
		driver.findElement(By.xpath("//input[@name='city']")).sendKeys(city);
		driver.findElement(By.xpath("//input[@name='state']")).sendKeys("MD");
		driver.findElement(By.xpath("//input[@name='pinno']")).sendKeys("021403");
		driver.findElement(By.xpath("//input[@name='telephoneno']")).sendKeys("21403");
		driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys(email);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pw);
		// Click Submit
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		// Get Customer ID
		String id = driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("=") + 1);
		// Edit Customer
		driver.findElement(By.xpath("//a[contains(text(),'Edit Customer')]")).click();
		// Input ID
		driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys(id);
		// Click Submit
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		// Verify Name and Address
		Assert.assertEquals(driver.findElement(By.xpath("//input[@name='name']")).getAttribute("value"), cname);
		Assert.assertEquals(driver.findElement(By.xpath("//textarea[@name='addr']")).getText(), address);
		WebElement wecity = driver.findElement(By.xpath("//input[@name='city']"));
		WebElement weaddress = driver.findElement(By.xpath("//textarea[@name='addr']"));
		// Input new value
		wecity.clear();
		weaddress.clear();
		wecity.sendKeys("new" + city);
		weaddress.sendKeys("new" + address);
		// Click Submit
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		// Verify new value
		Assert.assertEquals(
				driver.findElement(By.xpath("//td[contains(text(),'Address')]/following-sibling::td")).getText(),
				"new" + address);
		Assert.assertEquals(
				driver.findElement(By.xpath("//td[contains(text(),'City')]/following-sibling::td")).getText(),
				"new" + city);
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}
}
